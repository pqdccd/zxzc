Vue.component('joke-table', {
    props: {
        data: {
            required: true
        }
    },
    data() {
        return {
            clos: [],
            rows: []
        }
    },
    mounted() {
        this.clos = this.data.clos
        for (let i = 0; i < this.clos.length; i++) {
            this.clos[i].sorType = 'nomal'
        }
        this.rows = this.data.rows
    },
    methods: {
        order(type, key) {
            this.change(type, key)
        },
        change(type, key) {
            this.clos.forEach((item, i) => {
                if (item.key !== key) {
                    item.sorType = 'nomal'
                } else {
                    item.sorType = type
                    //动态改变
                    this.$set(this.clos,i,item)
                }
            })
            //排序
            this.rows.sort((a, b) => {
                if (type === 'asc') {
                    return a[key] > b[key] ? 1 : -1
                } else {
                    return a[key] > b[key] ? -1 : 1
                }
            })
        }
    },
    template: `
    <table class="joke-table">
            <tr>
                <th v-for="(item,index) in clos">
                {{item.title}}
                <span v-if="item.sortable">
                    <lable @click="order('asc',item.key)" class="order" :class="{active:item.sorType==='asc'}">↑</lable>
                    <lable  @click="order('desc',item.key)" class="order" :class="{active:item.sorType==='desc'}">↓</lable>  
                </span>
                </th>
            </tr>
            <tr v-for="item in rows">
                <td>{{item.id}}</td>
                <td>{{item.name}}</td>
                <td>{{item.age}}</td>
                <td>{{item.score}}</td>
            </tr>
    </table>
    `
})