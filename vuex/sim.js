; 

var SimbaPlugin = (function () {
    var SimbaPlugin = {}
    SimbaPlugin.install = function (Vue, options) {
        Vue.prototype.$sayHello = function () {
            console.log("hello")
        }

        Vue.sayHello = function (name) {
            console.log("hello", name)
        }

        //<myinput></myinput>
        Vue.component("myinput", {
            template: "<input />"
        })

        //v-focus 自定义指令
        Vue.directive("focus", {
            inserted(el) {
                el.focus()
            }
        })
    }

    Vue.use(SimbaPlugin)
    return SimbaPlugin
})()